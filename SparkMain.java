package SparkPowerCalculations;

public class SparkMain {

	public static void main(String[] args) 
	{
		SparkContextProvider.SetSparkContextParameters(false, false);
		
		TestAggregate sut = new TestAggregate();
		sut.RunLoadFlowTests();
		sut.RunMatrixIslandDetectionTests();
		//sut.RunGraphIslandDetectionTests();		
	}	
}
