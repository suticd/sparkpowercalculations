package SparkPowerCalculations.TestCases;

public class GridModel implements IGridModel 
{
	private static final long serialVersionUID = 3333767828005700418L;

	private int _dimension;

	private double[] _baseCase;
	
	private BusModel _busModel;   
    private BranchModel _branchModel;	
    
    public void Initialize(String filePath)
    {
        long startTime = System.nanoTime();
        
        FileParser fileParser = new FileParser();
        fileParser.Parse(filePath);
        
        _busModel = fileParser.GetBusModel();
        _busModel.SetFlatStartAssumption();
        
        _dimension = fileParser.GetDimension();
        
        _branchModel = fileParser.GetBranchModel();
        _branchModel.GenerateAdmittanceMatrix();

        long stopTime = System.nanoTime();
		
		System.out.println("Initializing test case took: " + (stopTime-startTime) + " ns. ");
    }
	
	@Override
	public BusModel GetBusModel()
	{
		return _busModel;
	}
    
    @Override
	public BranchModel GetBranchModel()
	{
		return _branchModel;
	}
	
	@Override
	public int GetDimension()
	{
		return _dimension;
	}
	
	@Override
	public void SetBaseCase(double[] baseCase)
	{
		_baseCase = new double[baseCase.length];
		
		System.arraycopy(baseCase, 0, _baseCase, 0, baseCase.length);
	}
	
	@Override
	public double[] GetBaseCase()
	{
		return _baseCase;
	}
}
