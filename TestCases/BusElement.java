package SparkPowerCalculations.TestCases;

import java.io.Serializable;

public class BusElement implements Serializable
{
	private static final long serialVersionUID = -7660274534382939980L;
	
	public int Id;
	public int Type;
	public double Pd;
	public double Qd;
	public double GShunt;
	public double BShunt;
	public double Pg;
	public double Qg;
	public double V;
	public double Theta;
}
