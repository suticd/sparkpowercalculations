package SparkPowerCalculations.TestCases;

import java.io.Serializable;

public class BusModel implements Serializable
{
	private static final long serialVersionUID = 8353255917152963109L;
	
	public static final int PQ_BUS_TYPE = 1;	
	public static final int PV_BUS_TYPE = 2;
	public static final int SLACK_BUS_TYPE = 3;
	
	private BusElement[] _buses;
	
	private int _numberOfBuses;
	private int _numberOfPVBuses;
	private int _numberOfSlackBuses;
	
	public BusModel(int numberOfBuses) 
	{
		_numberOfPVBuses = 0;
		_numberOfSlackBuses = 0;
		
		_numberOfBuses = numberOfBuses;
		_buses = new BusElement[_numberOfBuses];
	}
	
	public void Initialize(double[][] ParameterInput)
	{
		for (int i = 0; i < _numberOfBuses; i++) 
		{
			BusElement busElement = new BusElement();
			
			busElement.Id = (int)ParameterInput[i][0];
			busElement.Type = (int)ParameterInput[i][1];
			
			if (busElement.Type == PV_BUS_TYPE)			
			{
				_numberOfPVBuses++;
			}
			else if(busElement.Type == SLACK_BUS_TYPE)
			{
				_numberOfSlackBuses++;
			}
			
			busElement.Pd = ParameterInput[i][2] / 100;
			busElement.Qd = ParameterInput[i][3] / 100;
			busElement.Pg = ParameterInput[i][4] / 100;
			busElement.Qg = ParameterInput[i][5] / 100;
			busElement.V = ParameterInput[i][6];
			busElement.Theta = Math.toRadians(ParameterInput[i][7]);
			
			_buses[i] = busElement;
		}
	}
	
	public int GetNumberOfEquations()
	{
		return 2 * (_numberOfBuses - _numberOfSlackBuses) - _numberOfPVBuses;
	}
    
    public int GetNumberOfBuses()
    {
        return _numberOfBuses;
    }
    
    public BusElement GetBusElement(int busID)
    {
        return _buses[busID];
    }
    
    public void InsertBusElement(BusElement busElement, int index)
    {
        if (busElement.Type == PV_BUS_TYPE)			
        {
            _numberOfPVBuses++;
        }
        else if(busElement.Type == SLACK_BUS_TYPE)
        {
            _numberOfSlackBuses++;
        }
            
        _buses[index] = busElement;
    }
	
    public int GetBusID(int busID)
    {
    	return _buses[busID].Id;
    }
    
	public int GetBusType(int busID)
	{
		return _buses[busID].Type;
	}
	
	public double GetBusPd(int busID)
	{
		return _buses[busID].Pd;
	}
	
	public double GetBusQd(int busID)
	{
		return _buses[busID].Qd;
	}
	
	public double GetBusPg(int busID)
	{
		return _buses[busID].Pg;
	}
	
	public double GetBusQg(int busID)
	{
		return _buses[busID].Qg;
	}	
	
	public double GetBusShuntConductance(int busID)
	{
		return _buses[busID].GShunt;
	}
	
	public double GetBusShuntSusceptance(int busID)
	{
		return _buses[busID].BShunt;
	}
	
	public double GetBusVoltage(int busID)
	{
		return _buses[busID].V;
	}	
	
	public double GetBusAngle(int busID)
	{
		return _buses[busID].Theta;
	}	
    
    public double[] GetCurrentState(boolean includeFixedValues)
    {
        double[] currentState;
        int stateIndex = 0;
        
        if (includeFixedValues)
        {
            currentState = new double[_numberOfBuses];
            
            for (int i = 0; i < _numberOfBuses; i++) 
            {
                currentState[stateIndex++] = _buses[i].Theta;
                currentState[stateIndex++] = _buses[i].V;
            }
        }
        else
        {
            currentState = new double[GetNumberOfEquations()];
            
            for (int i = 0; i < _numberOfBuses; i++) 
            {
                if(_buses[i].Type == PV_BUS_TYPE)
                {
                    currentState[stateIndex++] = _buses[i].Theta;
                }
                else if(_buses[i].Type == PQ_BUS_TYPE)
                {
                    currentState[stateIndex++] = _buses[i].Theta;
                    currentState[stateIndex++] = _buses[i].V;
                }
            }
        }
        
        return currentState;
    }
	
	public void UpdateBusesWithCurrentSolution(double[] currentSolution, double maxAngleDeviation, double maxVoltageDeviation)
	{
		int solutionIterator = 0;
		
		for (int i = 0; i < _numberOfBuses; i++) 
		{
			if(_buses[i].Type == PV_BUS_TYPE)
			{
                UpdateBusAngle(i, currentSolution[solutionIterator++], maxAngleDeviation);
			}
			else if(_buses[i].Type == PQ_BUS_TYPE)
			{
				UpdateBusAngle(i, currentSolution[solutionIterator++], maxAngleDeviation);
				UpdateBusVoltage(i, currentSolution[solutionIterator++], maxVoltageDeviation);
			}
		}
	}
	
	public void SetFlatStartAssumption()
	{	
		for (int i = 0; i < _numberOfBuses; i++) 
		{
			if(_buses[i].Type == PV_BUS_TYPE)
			{
				_buses[i].Theta = 0; 
			}
			else if(_buses[i].Type == PQ_BUS_TYPE)
			{
				_buses[i].Theta = 0;//60; 				
				_buses[i].V = 1;//127017; 
			}
		}
	}
         
    private void UpdateBusAngle(int index, double newValue, double maxAngleDeviation)
    {
        if (Math.abs(newValue) > maxAngleDeviation)
        {
            _buses[index].Theta += Math.signum(newValue) * maxAngleDeviation;
        }
        else
        {
            _buses[index].Theta += newValue;
        }
    }
    
    private void UpdateBusVoltage(int index, double newValue, double maxVoltageDeviation)
    {
        if (Math.abs(newValue) > maxVoltageDeviation)
        {
            _buses[index].V += Math.signum(newValue) * maxVoltageDeviation;
        }
        else
        {
            _buses[index].V += newValue;
        }
    }
}
