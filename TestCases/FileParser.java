package SparkPowerCalculations.TestCases;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

public class FileParser
{
    private static final int BUS_ID_INDEX = 0;
    private static final int BUS_TYPE_INDEX = 1;
    private static final int PD_INDEX = 2;
    private static final int QD_INDEX = 3;
    private static final int G_SHUNT_INDEX = 4;
    private static final int B_SHUNT_INDEX = 5;
    private static final int VOLTAGE_INDEX = 7;
    private static final int ANGLE_INDEX = 8;
    
    private static final int PG_INDEX = 1;
    private static final int QG_INDEX = 2;
    
    private static final int FROM_BUS_INDEX = 0;
    private static final int TO_BUS_INDEX = 1;
    private static final int R_INDEX = 2;
    private static final int X_INDEX = 3;
    private static final int B_INDEX = 4;
    private static final int TAP_RATIO_INDEX = 8;
    private static final int PHASE_SHIFT_INDEX = 9;
    private static final int BRANCH_STATUS_INDEX = 10;
    
    private int _dimension;
    private double _baseMVA;
    private HashMap<Integer, Integer> _busIDIndexMap = new HashMap<Integer, Integer>();
      
    private int _busElementIndex;
    private int _branchElementIndex;
    private int _numberOfBranchConnections;
    
    private BusModel _busModel;
    private BranchModel _branchModel;
    
    public void Parse(String filePath)
    {
    	InputStream inputStream = FileParser.class.getResourceAsStream(filePath);
		
        // sections: 0 - dimension, 1 - base MVA, 2 - bus data, 3 - number of branch connections, 4 - generator data, 5 - branch data
        int stateCounter = 0;
	    
	    try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream)))
	    {
			String line = null;
			  
			while ((line = reader.readLine()) != null) 
			{    			
                // skip comments
                if (line.startsWith("%"))
                {
                    continue;
                }
                
                // move to next section
                if (line.startsWith(";"))
                {
                    stateCounter++;
                    continue;
                }
                
                // interpret read data based on the current section
                switch (stateCounter)
                {
                    case 0:
                    {
                        _dimension = Integer.parseInt(line);
                        _busModel = new BusModel(_dimension);
                        _busElementIndex = 0;
                        break;
                    }
                    case 1:
                    {
                        _baseMVA = Double.parseDouble(line);
                        break;
                    }
                    case 2:
                    {
                        _numberOfBranchConnections = Integer.parseInt(line);
                        _branchModel = new BranchModel(_numberOfBranchConnections, _busModel);
                        _branchElementIndex = 0;
                        break;
                    }
                    case 3:
                    {
                        ParseBusDataLine(line);
                        break;
                    } 
                    case 4:
                    {
                        ParseGeneratorDataLine(line);
                        break;
                    }
                    case 5:
                    {
                        ParseBranchDataLine(line);
                        break;
                    }
                }
			}      
	    }
	    catch (IOException e)
	    {
	    }
    }
    
    public BusModel GetBusModel()
    {
        return _busModel;
    }
    
    public BranchModel GetBranchModel()
    {
        return _branchModel;
    }
    
    public int GetDimension()
    {
        return _dimension;
    }
    
    private void ParseBusDataLine(String line)
    {
        BusElement busElement = new BusElement();
        String[] result = line.split(",");
        
        busElement.Id = Integer.parseInt(result[BUS_ID_INDEX]);
        _busIDIndexMap.put(busElement.Id, _busElementIndex);
        
        busElement.Type = Integer.parseInt(result[BUS_TYPE_INDEX]);
        busElement.Pd = Double.parseDouble(result[PD_INDEX]) / _baseMVA;
        busElement.Qd = Double.parseDouble(result[QD_INDEX]) / _baseMVA;
        busElement.GShunt = Double.parseDouble(result[G_SHUNT_INDEX]) / _baseMVA;
        busElement.BShunt = Double.parseDouble(result[B_SHUNT_INDEX]) / _baseMVA;
        busElement.V = Double.parseDouble(result[VOLTAGE_INDEX]);
        busElement.Theta = Math.toRadians(Double.parseDouble(result[ANGLE_INDEX]));
        
        _busModel.InsertBusElement(busElement, _busElementIndex++);
    }
    
    private void ParseGeneratorDataLine(String line)
    {
        String[] result = line.split(",");
        int busId = Integer.parseInt(result[BUS_ID_INDEX]);
        
        BusElement busElement = _busModel.GetBusElement(_busIDIndexMap.get(busId));
        
        busElement.Pg += Double.parseDouble(result[PG_INDEX]) / _baseMVA;
        busElement.Qg += Double.parseDouble(result[QG_INDEX]) / _baseMVA;
    }
    
    private void ParseBranchDataLine(String line)
    {
        BranchElement branchElement = new BranchElement();
        String[] result = line.split(",");

        branchElement.FromBus = _busIDIndexMap.get(Integer.parseInt(result[FROM_BUS_INDEX]));
        branchElement.ToBus = _busIDIndexMap.get(Integer.parseInt(result[TO_BUS_INDEX]));
        branchElement.R = Double.parseDouble(result[R_INDEX]);
        branchElement.X = Double.parseDouble(result[X_INDEX]);
        branchElement.B = Double.parseDouble(result[B_INDEX]);
        branchElement.TapRatio = Double.parseDouble(result[TAP_RATIO_INDEX]);
        branchElement.PhaseShiftAngle = Math.toRadians(Double.parseDouble(result[PHASE_SHIFT_INDEX]));
        branchElement.BranchStatus = Integer.parseInt(result[BRANCH_STATUS_INDEX]);
        
        _branchModel.InsertBranchElement(branchElement, _branchElementIndex++);
    }
}