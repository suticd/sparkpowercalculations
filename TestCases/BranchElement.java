package SparkPowerCalculations.TestCases;

public class BranchElement 
{
    public int FromBus;
    public int ToBus;
    public double R;
    public double X;
    public double B;
    public double TapRatio;
    public double PhaseShiftAngle;
    public int BranchStatus;
}
