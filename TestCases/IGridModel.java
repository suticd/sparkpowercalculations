package SparkPowerCalculations.TestCases;

import java.io.Serializable;

public interface IGridModel extends Serializable 
{		
	public void Initialize(String filePath);	
	
	public BusModel GetBusModel();
    
    public BranchModel GetBranchModel();
	
	public int GetDimension();

	public void SetBaseCase(double[] baseCase);
	
	public double[] GetBaseCase();	
}
