package SparkPowerCalculations.TestCases;

import org.apache.commons.math3.linear.OpenMapRealMatrix;

public class BranchModel 
{
    private BranchElement[] _branches;
    private int _numberOfBranches;
    
    private BusModel _busModel;
    
    private OpenMapRealMatrix _gMatrix;
    private OpenMapRealMatrix _bMatrix;
    
    public BranchModel (int numberOfBranches, BusModel busModel)
    {
        _numberOfBranches = numberOfBranches;        
        _branches = new BranchElement[_numberOfBranches];
        
        _busModel = busModel;
    }
    
    public void InsertBranchElement(BranchElement branchElement, int index)
    {
        _branches[index] = branchElement;
    }
            
    public double GetBranchG(int fromBus, int toBus)
    {
        return _gMatrix.getEntry(fromBus, toBus);
    }
    
    public double GetBranchB(int fromBus, int toBus)
    {
        return _bMatrix.getEntry(fromBus, toBus);
    }
    
    public BranchElement[] GetBranches()
    {
    	return _branches;
    }
    
    public void CreateOutage(int fromBus, int toBus)
    {
        if (fromBus == toBus)
        {
            // error
            return;
        }
        
        for (int i = 0; i < _numberOfBranches; i++) 
		{
            if (_branches[i].FromBus == fromBus && _branches[i].ToBus == toBus)
            {
                _branches[i].BranchStatus = 0;
                break;
            }
        }
        
        GenerateAdmittanceMatrix();
    }
    
    /**
	 * Poplates the G and B matrices with the conductance and susceptance information.
	 * 
	 * Iterate through the input list. For each node (from -> to) fill the non-diagonal element of the G and B matrices with the negative values. 
	 * Maintain two arrays in which the collective sum of R and X is stored for both "from" and "to" nodes.
	 * Iterate through both G and B matrices and fill them with the values generated from the arrays from above.
	 */
    public void GenerateAdmittanceMatrix()
    {            
        _gMatrix = new OpenMapRealMatrix(_busModel.GetNumberOfBuses(), _busModel.GetNumberOfBuses());
        _bMatrix = new OpenMapRealMatrix(_busModel.GetNumberOfBuses(), _busModel.GetNumberOfBuses());
        
        double G, B;
        
        BranchElement branchElement;
		
		double[] selfBusG = new double[_busModel.GetNumberOfBuses()];
		double[] selfBusB = new double[_busModel.GetNumberOfBuses()];
				
		for (int i = 0; i < _numberOfBranches; i++) 
		{
            branchElement = _branches[i];
            
            if (branchElement.BranchStatus == 0)
            {
                continue;
            }
			
			G = branchElement.R / (branchElement.R * branchElement.R + branchElement.X * branchElement.X);
			B = -branchElement.X / (branchElement.R * branchElement.R + branchElement.X * branchElement.X);			
			
			if (branchElement.TapRatio == 0)
			{
                _gMatrix.addToEntry(branchElement.FromBus, branchElement.ToBus, -G);
                _gMatrix.addToEntry(branchElement.ToBus, branchElement.FromBus, -G);
                
                _bMatrix.addToEntry(branchElement.FromBus, branchElement.ToBus, -B);
                _bMatrix.addToEntry(branchElement.ToBus, branchElement.FromBus, -B);
                
                selfBusG[branchElement.FromBus] += G;
                selfBusG[branchElement.ToBus] += G;
                
                selfBusB[branchElement.FromBus] += B + branchElement.B / 2;
                selfBusB[branchElement.ToBus] += B + branchElement.B / 2;
			}
			else
			{
				_gMatrix.addToEntry(branchElement.FromBus, branchElement.ToBus, -(G * Math.cos(branchElement.PhaseShiftAngle) - B * Math.sin(branchElement.PhaseShiftAngle)) / branchElement.TapRatio);
				_gMatrix.addToEntry(branchElement.ToBus, branchElement.FromBus, -(G * Math.cos(branchElement.PhaseShiftAngle) + B * Math.sin(branchElement.PhaseShiftAngle)) / branchElement.TapRatio);

				_bMatrix.addToEntry(branchElement.FromBus, branchElement.ToBus, -(G * Math.sin(branchElement.PhaseShiftAngle) + B * Math.cos(branchElement.PhaseShiftAngle)) / branchElement.TapRatio);
				_bMatrix.addToEntry(branchElement.ToBus, branchElement.FromBus, -(B * Math.cos(branchElement.PhaseShiftAngle) - G * Math.sin(branchElement.PhaseShiftAngle)) / branchElement.TapRatio);
                
                selfBusG[branchElement.FromBus] += G / (branchElement.TapRatio * branchElement.TapRatio);
                selfBusG[branchElement.ToBus] += G;
                
                selfBusB[branchElement.FromBus] += (B + branchElement.B / 2) / (branchElement.TapRatio * branchElement.TapRatio);
                selfBusB[branchElement.ToBus] += B + branchElement.B / 2;
			}
		}

		for (int busId = 0; busId < _busModel.GetNumberOfBuses(); busId++) 
		{
            _gMatrix.setEntry(busId, busId, selfBusG[busId] + _busModel.GetBusShuntConductance(busId));
            _bMatrix.setEntry(busId, busId, selfBusB[busId] + _busModel.GetBusShuntSusceptance(busId));
		}
    }
}
