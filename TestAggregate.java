package SparkPowerCalculations;

import SparkPowerCalculations.ContingencyAnalysis.DistributedGraphTopologyAnalizerTests;
import SparkPowerCalculations.ContingencyAnalysis.DistributedMatrixTopologyAnalizerTests;
import SparkPowerCalculations.PowerFlow.PowerFlowEngineTests;

public class TestAggregate 
{
	public void RunGraphIslandDetectionTests()
	{
		System.out.println("*** Graph Topology Analizer ***");
		DistributedGraphTopologyAnalizerTests sut = new DistributedGraphTopologyAnalizerTests();
		
		System.out.println(" -- Starting case 4...");
		sut.NoOutagesCommonTest(4);
		System.out.println(" -- Case 4 finished.");
		
		System.out.println(" -- Starting case 5...");
		sut.NoOutagesCommonTest(5);
		System.out.println(" -- Case 5 finished.");
		
		System.out.println(" -- Starting case 6...");
		sut.NoOutagesCommonTest(6);
		System.out.println(" -- Case 6 finished.");
		
		System.out.println(" -- Starting case 9...");
		sut.NoOutagesCommonTest(9);
		System.out.println(" -- Case 9 finished.");
		
		System.out.println(" -- Starting case 14...");
		sut.NoOutagesCommonTest(14);
		System.out.println(" -- Case 14 finished.");
		
		System.out.println(" -- Starting case 24...");
		sut.NoOutagesCommonTest(24);
		System.out.println(" -- Case 24 finished.");
		
		System.out.println(" -- Starting case 30...");
		sut.NoOutagesCommonTest(30);
		System.out.println(" -- Case 30 finished.");
		
		System.out.println(" -- Starting case 39...");
		sut.NoOutagesCommonTest(39);
		System.out.println(" -- Case 39 finished.");
		
		System.out.println(" -- Starting case 57...");
		sut.NoOutagesCommonTest(57);
		System.out.println(" -- Case 57 finished.");
		
		System.out.println(" -- Starting case 89...");
		sut.NoOutagesCommonTest(89);
		System.out.println(" -- Case 89 finished.");
		
		System.out.println(" -- Starting case 118...");
		sut.NoOutagesCommonTest(118);
		System.out.println(" -- Case 118 finished.");
		
		System.out.println(" -- Starting case 300...");
		sut.NoOutagesCommonTest(300);
		System.out.println(" -- Case 300 finished.");
		
		System.out.println(" -- Starting case 1354...");
		sut.NoOutagesCommonTest(1354);
		System.out.println(" -- Case 1354 finished.");
		
		System.out.println(" -- Starting case 2383...");
		sut.NoOutagesCommonTest(2383);
		System.out.println(" -- Case 2383 finished.");	
		
		System.out.println(" -- Starting case 2736...");
		sut.NoOutagesCommonTest(2736);
		System.out.println(" -- Case 2736 finished.");
		
		System.out.println(" -- Starting case 2746...");
		sut.NoOutagesCommonTest(2746);
		System.out.println(" -- Case 2746 finished.");
		
		System.out.println(" -- Starting case 2869...");
		sut.NoOutagesCommonTest(2869);
		System.out.println(" -- Case 2869 finished.");
		
		System.out.println(" -- Starting case 3012...");
		sut.NoOutagesCommonTest(3012);
		System.out.println(" -- Case 3012 finished.");
		
		System.out.println(" -- Starting case 3120...");
		sut.NoOutagesCommonTest(3120);
		System.out.println(" -- Case 3120 finished.");
		
		System.out.println(" -- Starting case 3374...");
		sut.NoOutagesCommonTest(3374);
		System.out.println(" -- Case 3374 finished.");
		
		System.out.println(" -- Starting case 9241...");
		sut.NoOutagesCommonTest(9241);
		System.out.println(" -- Case 9241 finished.");				
	}
	
	public void RunMatrixIslandDetectionTests()
	{
 		System.out.println("*** Matrix Topology Analizer ***");
 		DistributedMatrixTopologyAnalizerTests sut = new DistributedMatrixTopologyAnalizerTests();
		
		System.out.println(" -- Starting case 4...");
		sut.NoOutagesCommonTest(4);
		System.out.println(" -- Case 4 finished.");
		
		System.out.println(" -- Starting case 5...");
		sut.NoOutagesCommonTest(5);
		System.out.println(" -- Case 5 finished.");
		
		System.out.println(" -- Starting case 6...");
		sut.NoOutagesCommonTest(6);
		System.out.println(" -- Case 6 finished.");
		
		System.out.println(" -- Starting case 9...");
		sut.NoOutagesCommonTest(9);
		System.out.println(" -- Case 9 finished.");
		
		System.out.println(" -- Starting case 14...");
		sut.NoOutagesCommonTest(14);
		System.out.println(" -- Case 14 finished.");
		
		System.out.println(" -- Starting case 24...");
		sut.NoOutagesCommonTest(24);
		System.out.println(" -- Case 24 finished.");
		
		System.out.println(" -- Starting case 30...");
		sut.NoOutagesCommonTest(30);
		System.out.println(" -- Case 30 finished.");
		
		System.out.println(" -- Starting case 39...");
		sut.NoOutagesCommonTest(39);
		System.out.println(" -- Case 39 finished.");
		
		System.out.println(" -- Starting case 57...");
		sut.NoOutagesCommonTest(57);
		System.out.println(" -- Case 57 finished.");
		
		System.out.println(" -- Starting case 89...");
		sut.NoOutagesCommonTest(89);
		System.out.println(" -- Case 89 finished.");
		
		System.out.println(" -- Starting case 118...");
		sut.NoOutagesCommonTest(118);
		System.out.println(" -- Case 118 finished.");
		
		System.out.println(" -- Starting case 300...");
		sut.NoOutagesCommonTest(300);
		System.out.println(" -- Case 300 finished.");
		
		System.out.println(" -- Starting case 1354...");
		sut.NoOutagesCommonTest(1354);
		System.out.println(" -- Case 1354 finished.");
		
		System.out.println(" -- Starting case 2383...");
		sut.NoOutagesCommonTest(2383);
		System.out.println(" -- Case 2383 finished.");	
		
		System.out.println(" -- Starting case 2736...");
		sut.NoOutagesCommonTest(2736);
		System.out.println(" -- Case 2736 finished.");
		
		System.out.println(" -- Starting case 2746...");
		sut.NoOutagesCommonTest(2746);
		System.out.println(" -- Case 2746 finished.");
		
		System.out.println(" -- Starting case 2869...");
		sut.NoOutagesCommonTest(2869);
		System.out.println(" -- Case 2869 finished.");
		
		System.out.println(" -- Starting case 3012...");
		sut.NoOutagesCommonTest(3012);
		System.out.println(" -- Case 3012 finished.");
		
		System.out.println(" -- Starting case 3120...");
		sut.NoOutagesCommonTest(3120);
		System.out.println(" -- Case 3120 finished.");
		
		System.out.println(" -- Starting case 3374...");
		sut.NoOutagesCommonTest(3374);
		System.out.println(" -- Case 3374 finished.");
		
		System.out.println(" -- Starting case 9241...");
		sut.NoOutagesCommonTest(9241);
		System.out.println(" -- Case 9241 finished.");				
	}

	public void RunLoadFlowTests()
	{
		System.out.println("*** Power Flow ***");
		
		PowerFlowEngineTests sut = new PowerFlowEngineTests();
		sut.Run_All();
	}
}
