package SparkPowerCalculations;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Logger 
{
	//private final static String LOG_DICTIONARY_PATH = "S:\\Users\\Davor\\Documents\\Eclipse\\workspace\\SparkPowerCalculations\\src\\SparkPowerCalculations\\TestCases\\log\\";
	private final static String LOG_DICTIONARY_PATH = "s3:\\aws-logs-704304477029-us-east-1\\elasticmapreduce/";
	private static PrintWriter _writer;
	
	public static void SetLogFile(String fileName)
	{
		if(_writer != null)
		{
			_writer.close();
		}
		
		try		
		{
			//String filePath = Logger.class.getResource("TestCases/log/" + fileName).getPath(); "TestCases/log/"
			_writer = new PrintWriter(LOG_DICTIONARY_PATH + fileName + ".txt", "UTF-8");
		} 
		catch (FileNotFoundException | UnsupportedEncodingException e) 
		{
			e.printStackTrace();
		}	
	}
	
	public static void WriteLog(String log)
	{
		String timeStamp = new SimpleDateFormat("HH:mm:ss.SSS").format(Calendar.getInstance().getTime());
		_writer.print(timeStamp + " - " + log);
		_writer.flush();
	}
	
	public static void WriteLogLine(String log)
	{
		String timeStamp = new SimpleDateFormat("HH:mm:ss.SSS").format(Calendar.getInstance().getTime());
		_writer.println(timeStamp + " - " + log);
		_writer.flush();
	}
	
	public static void PrintArray(String prefix, double[] array)
	{
		_writer.print("\t\t" + prefix + " is:\n\t\t\t\t");
		
		for (int i = 0; i < array.length; i++) 
		{
			_writer.print(array[i] + ", ");
		}
		
		_writer.println();
	}
	
	public static void PrintMatrixToFile(double[][] matrix, String fileName)
	{
		PrintWriter writer;
		
		try 
		{
			writer = new PrintWriter(LOG_DICTIONARY_PATH + fileName + ".txt", "UTF-8");
			
			for (int i = 0; i < matrix.length; i++) 
			{
				for (int j = 0; j < matrix.length; j++) 
				{
					writer.print(matrix[i][j]);
					writer.print(",");
				}
				
				writer.println();
			}
			
			writer.close();
		} 
		catch (FileNotFoundException | UnsupportedEncodingException e) 
		{
			e.printStackTrace();
		}		
	}
}
