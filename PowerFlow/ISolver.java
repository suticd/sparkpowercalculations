package SparkPowerCalculations.PowerFlow;

public interface ISolver 
{
	/**
	 * Solves the matrix equation Ax = b.
	 * 1. LUx = b
	 * 2. Ly = b
	 * 3. Ux = y
	 * @param A
	 * @param b
	 * @return The solution x
	 */
	void Solve(double[][] A, double[] x, double[] b);
}
