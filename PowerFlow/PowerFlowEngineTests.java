package SparkPowerCalculations.PowerFlow;

import org.junit.Assert;
import org.junit.Test;

import SparkPowerCalculations.TestCases.GridModel;

public class PowerFlowEngineTests 
{
    @Test
    public void Run_All()
    {
    	System.out.println("CASE: 4 bus");
    	ExeuteLoadFlowTest_4bus();
    	
    	System.out.println("\n\nCASE: 5 bus");
    	ExeuteLoadFlowTest_5bus();
    	
    	System.out.println("\n\nCASE: 6 bus");
    	ExeuteLoadFlowTest_6bus();
    	
    	System.out.println("\n\nCASE: 9 bus");
    	ExeuteLoadFlowTest_9bus();
    	
    	System.out.println("\n\nCASE: 14 bus");
    	ExeuteLoadFlowTest_14bus();
    	
    	System.out.println("\n\nCASE: 24 bus");
    	ExeuteLoadFlowTest_24bus();
    	
    	System.out.println("\n\nCASE: 30 bus");
    	ExeuteLoadFlowTest_30bus();
    	
    	System.out.println("\n\nCASE: 39 bus");
    	ExeuteLoadFlowTest_39bus();
    	
    	System.out.println("\n\nCASE: 57 bus");
    	ExeuteLoadFlowTest_57bus();
    	
    	System.out.println("\n\nCASE: 89 bus");
    	ExeuteLoadFlowTest_89bus();
    	
    	System.out.println("\n\nCASE: 118 bus");
    	ExeuteLoadFlowTest_118bus();
    	
    	System.out.println("\n\nCASE: 300 bus");
    	ExeuteLoadFlowTest_300bus();
    	
    	System.out.println("\n\nCASE: 1354 bus");
    	ExeuteLoadFlowTest_1354bus();
    	
    	System.out.println("\n\nCASE: 2383 bus");
    	ExeuteLoadFlowTest_2383bus();
    	
//    	System.out.println("\n\nCASE: 2736 bus");
//    	ExeuteLoadFlowTest_2736bus();
//    	
//    	//System.out.println("\n\nCASE: 2746 bus");
//    	//ExeuteLoadFlowTest_2746bus();
//    	
//    	System.out.println("\n\nCASE: 2869 bus");
//    	ExeuteLoadFlowTest_2869bus();
//    	
//    	System.out.println("\n\nCASE: 3012 bus");
//    	ExeuteLoadFlowTest_3012bus();
//    	
//    	System.out.println("\n\nCASE: 3120 bus");
//    	ExeuteLoadFlowTest_3120bus();
    	
    	System.out.println("\n\nCASE: 3374 bus");
    	ExeuteLoadFlowTest_3374bus();
    	
//    	System.out.println("\n\nCASE: 9241 bus");
//    	ExeuteLoadFlowTest_9241bus();
    }
    
	@Test
	public void ExeuteLoadFlowTest_4bus() 
	{
		ExecuteTest("case_4.m");
	}
	
	@Test
	public void ExeuteLoadFlowTest_5bus() 
	{
		ExecuteTest("case_5.m");
	}
	
	@Test
	public void ExeuteLoadFlowTest_6bus() 
	{
		ExecuteTest("case_6.m");
	}
	
	@Test
	public void ExeuteLoadFlowTest_9bus() 
	{
		ExecuteTest("case_9.m");
	}
	
	@Test
	public void ExeuteLoadFlowTest_14bus() 
	{
		ExecuteTest("case_14.m");
	}
	
	@Test
	public void ExeuteLoadFlowTest_24bus() 
	{
		ExecuteTest("case_24.m");
	}
	
	@Test
	public void ExeuteLoadFlowTest_30bus() 
	{
		ExecuteTest("case_30.m");
	}
	
	@Test
	public void ExeuteLoadFlowTest_39bus() 
	{
		ExecuteTest("case_39.m");
	}
	
	@Test
	public void ExeuteLoadFlowTest_57bus() 
	{
		ExecuteTest("case_57.m");
	}
	
	@Test
	public void ExeuteLoadFlowTest_89bus() 
	{
		ExecuteTest("case_89.m");
	}
	
	@Test
	public void ExeuteLoadFlowTest_118bus() 
	{
		ExecuteTest("case_118.m");
	}

	@Test
	public void ExeuteLoadFlowTest_300bus() 
	{
		ExecuteTest("case_300.m");
	}
	
	@Test
	public void ExeuteLoadFlowTest_1354bus() 
	{
		ExecuteTest("case_1354.m");
	}
	
	@Test
	public void ExeuteLoadFlowTest_2383bus() 
	{
		ExecuteTest("case_2383.m");
	}
	
	@Test
	public void ExeuteLoadFlowTest_2736bus() 
	{
		ExecuteTest("case_2736.m");
	}
    
    @Test
	public void ExeuteLoadFlowTest_2746bus() 
	{
		ExecuteTest("case_2746.m");
	}
    
    @Test
	public void ExeuteLoadFlowTest_2869bus() 
	{
		ExecuteTest("case_2869.m");
	}
    
    @Test
	public void ExeuteLoadFlowTest_3012bus() 
	{
		ExecuteTest("case_3012.m");
	}
    
    @Test
	public void ExeuteLoadFlowTest_3120bus() 
	{
		ExecuteTest("case_3120.m");
	}
    
    @Test
	public void ExeuteLoadFlowTest_3374bus() 
	{
		ExecuteTest("case_3374.m");
	}
    
    @Test
	public void ExeuteLoadFlowTest_9241bus() 
	{
		ExecuteTest("case_9241.m");
	}
    
    private void ExecuteTest(String fileName)
    {
    	//Logger.SetLogFile(fileName + "_log");
        String filePath = "/SparkPowerCalculations/TestCases/data_files/" + fileName;

        GridModel testCase = new GridModel();
		testCase.Initialize(filePath);
		
		PowerFlowEngine sut = new PowerFlowEngine(new DistributedSolver());
		double[] result = sut.ExecuteLoadFlowCalculation(testCase);
		Assert.assertNotNull(result);
    }
}
