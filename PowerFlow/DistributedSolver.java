package SparkPowerCalculations.PowerFlow;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.linalg.Matrices;
import org.apache.spark.mllib.linalg.Matrix;
import org.apache.spark.mllib.linalg.SingularValueDecomposition;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import scala.Tuple2;
import org.apache.spark.mllib.linalg.distributed.IndexedRowMatrix;

import SparkPowerCalculations.SparkContextProvider;

import org.apache.spark.mllib.linalg.distributed.CoordinateMatrix;
import org.apache.spark.mllib.linalg.distributed.IndexedRow;


public class DistributedSolver implements ISolver 
{

	@Override
	public void Solve(double[][] A, double[] x, double[] b) 
	{
		long initSparkContextStart = System.nanoTime();
		JavaSparkContext sc = SparkContextProvider.GetSparkContext();
		long initSparkContextStop = System.nanoTime();
		
		System.out.println("\t\t\tInitializing Spark Context, time spent: " + (initSparkContextStop - initSparkContextStart) + " ns. ");
		
		long prepareDistributedMatrixStart = System.nanoTime();
		LinkedList<IndexedRow> rowsList = new LinkedList<>();

	    for (int i = 0; i < A.length; i++) 
	    {
	        Vector currentRow = ArrayToVector(A[i]);//Vectors.dense(A[i]);
	        rowsList.add(new IndexedRow(i, currentRow));
	    }
	    
	    long prepareDistributedMatrixStop = System.nanoTime();
	    System.out.println("\t\t\tPreparing Distributed matrix, time spent: " + (prepareDistributedMatrixStop - prepareDistributedMatrixStart) + " ns. ");
	    
	    JavaRDD<IndexedRow> rows = sc.parallelize(rowsList, 100);
	    
	    IndexedRowMatrix mat = new IndexedRowMatrix(rows.rdd());
	    
	    // x = V * S^-1 * Ut * b
	    
	    SingularValueDecomposition<IndexedRowMatrix, Matrix> svd = mat.computeSVD(A.length, true, 1.0E-19d);
	    IndexedRowMatrix Utransposed = Transpose(svd.U());
	    Vector s = svd.s();
	    Matrix V = svd.V();
	    
	    Matrix bToMatrix = Matrices.dense(b.length, 1, b);
	    IndexedRowMatrix UtimesB = Utransposed.multiply(bToMatrix);

	    JavaRDD<Tuple2<Integer, Double>> tupleSet = UtimesB.rows().toJavaRDD().map(i -> new Tuple2<Integer, Double>((int)i.index(), i.vector().apply(0) / s.apply((int)i.index())));

	    String tupleSetDebugString = tupleSet.toDebugString();
	    
	    List<Tuple2<Integer, Double>> tupleList = tupleSet.collect();
	    
	    long prepareResultStart = System.nanoTime();
	    Vector v = ListToVector(tupleList);
	    
	    Vector result = V.multiply(v);
	    
	    System.arraycopy(result.toArray(), 0, x, 0, x.length);
	    long prepareResultStop = System.nanoTime();
	    System.out.println("\t\t\tPreparing result, time spent: " + (prepareResultStop - prepareResultStart) + " ns. ");
	    System.out.println("\t\t\tTupleSetDebugString: " + tupleSetDebugString);
	}
	
	private IndexedRowMatrix Transpose(IndexedRowMatrix matrix)
	{
		CoordinateMatrix cmat = matrix.toCoordinateMatrix().transpose();
		return cmat.toIndexedRowMatrix();
	}
	
	private Vector ListToVector(List<Tuple2<Integer, Double>> tupleList)
	{			
		double[] retValue = new double[tupleList.size()];
		
		for (Tuple2<Integer, Double> tuple : tupleList) 
		{
			retValue[tuple._1] = tuple._2;
		}
				
		return Vectors.dense(retValue);
	}
	
	private Vector ArrayToVector(double[] array)
	{		
		List<Integer> indexList = new ArrayList<Integer>();
		List<Double> valueList = new ArrayList<Double>();
		
		for (int i = 0; i < array.length; i++) 
		{
			if(array[i] != 0.0)
			{
				indexList.add(i);
				valueList.add(array[i]);
			}
		}
		
				
		return Vectors.sparse(array.length, indexList.stream().mapToInt(i->i).toArray(), valueList.stream().mapToDouble(i->i).toArray());
	}
}
