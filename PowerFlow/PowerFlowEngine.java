package SparkPowerCalculations.PowerFlow;

import SparkPowerCalculations.TestCases.BusModel;
import SparkPowerCalculations.TestCases.BranchModel;
import SparkPowerCalculations.TestCases.IGridModel;

public class PowerFlowEngine 
{
	private static final int MAX_ITERATIONS = 15;
	private static final double EPSILON_VOLTAGE = 1E-3;
	private static final double EPSILON_ANGLE = 1E-3;
	private static final double MAX_VOLTAGE_DEVIATION = 0.07;
	private static final double MAX_ANGLE_DEVIATION = 0.7;
	
	private double[][] _jacobian;
	private double[] _deltaS;
	private int _busCount;
	
	private ISolver _solver;
	
	private BusModel _busModel;
    private BranchModel _branchModel;
	private int _numberOfEquations;
	
	public PowerFlowEngine(ISolver solver)
	{
		_solver = solver;		
	}
	
	public double[] ExecuteLoadFlowCalculation(IGridModel gridModel)
	{
		long jacobianStart, jacobianStop, jacobianTotal = 0;
		long solverStart, solverStop, solverTotal = 0;
		
		long startTime = System.nanoTime();
		
		SetInputParameters(gridModel);
		
		double[] currentSolution = new double[_numberOfEquations];
					
		for (int i = 0; i < MAX_ITERATIONS; i++) 
		{
            // populate jacobian
			jacobianStart = System.nanoTime();
			PopulateJacobian();
			jacobianStop = System.nanoTime();
			System.out.println("\t\tJacobian matrix generated (Iteration " + (i+1) + "), time spent: " + (jacobianStop - jacobianStart) + " ns. ");
			//Logger.WriteLogLine("\t\tJacobian matrix generated (Iteration " + (i+1) + "), time spent: " + (jacobianStop - jacobianStart) + " ns. ");
			jacobianTotal += jacobianStop - jacobianStart;
			
            // populate delta S
			PopulateDeltaS();
			
            // solve system of equations
			solverStart = System.nanoTime();
			_solver.Solve(_jacobian, currentSolution, _deltaS);
			solverStop = System.nanoTime();
			System.out.println("\t\tSolved matrix equation (Iteration " + (i+1) + "), time spent: " + (solverStop - solverStart) + " ns. ");
			//Logger.WriteLogLine("\t\tSolved matrix equation (Iteration " + (i+1) + "), time spent: " + (solverStop - solverStart) + " ns. ");
			solverTotal += solverStop - solverStart;			
			
            // check whether solution converged
			if(SolutionFound(currentSolution))
			{
				long stopTime = System.nanoTime();
				_busModel.UpdateBusesWithCurrentSolution(currentSolution, MAX_ANGLE_DEVIATION, MAX_VOLTAGE_DEVIATION);
				//Logger.PrintArray("--> Final solution", _busModel.GetCurrentState(false));
				System.out.println("Executing load flow finished in " + (i+1) + " iterations and the calculation took: " + (stopTime-startTime) + " ns. ");
				//Logger.WriteLogLine("Executing load flow finished in " + (i+1) + " iterations and the calculation took: " + (stopTime-startTime) + " ns. ");
				System.out.println("\tTotal time spent generating Jacobian matrix: " + jacobianTotal + " ns. ");
				//Logger.WriteLogLine("\tTotal time spent generating Jacobian matrix: " + jacobianTotal + " ns. ");
				System.out.println("\tTotal time spent solving matrix equation: " + solverTotal + " ns. ");
				//Logger.WriteLogLine("\tTotal time spent solving matrix equation: " + solverTotal + " ns. ");
				return _busModel.GetCurrentState(false);
			}
			
            // update current assumption
			//Logger.PrintArray("Solution", currentSolution);
			_busModel.UpdateBusesWithCurrentSolution(currentSolution, MAX_ANGLE_DEVIATION, MAX_VOLTAGE_DEVIATION);
		}
		
		System.out.println("Load flow calculation didn't converge after " + MAX_ITERATIONS + " iterations.");
		//Logger.WriteLogLine("Load flow calculation didn't converge after " + MAX_ITERATIONS + " iterations.");
		return null;
	}
	
	private double CheckNumber(double x, int i, int j)
	{
		if(Double.isInfinite(x))
		{
			//Logger.WriteLogLine("Number is infinity: i= " + i + " j= " + j);
		}
		
		if(Double.isNaN(x))
		{
			//Logger.WriteLogLine("Number is NaN: i= " + i + " j= " + j);
		}
		
		if (x == -0.0)
		{
			return 0.0;
		}
		
		return x;
	}	
	
	public boolean SolutionFound(double[] currentSolution)
	{	
    	int solutionIndex = 0;
		
		for (int busID = 0; busID < _busCount; busID++) 
		{
			if (_busModel.GetBusType(busID) == BusModel.PQ_BUS_TYPE)
			{
				if (Math.abs(currentSolution[solutionIndex++]) > EPSILON_ANGLE || Math.abs(currentSolution[solutionIndex++]) > EPSILON_VOLTAGE)
                {
                    return false;
                }	
			}
			else if (_busModel.GetBusType(busID) == BusModel.PV_BUS_TYPE)
			{
				if (Math.abs(currentSolution[solutionIndex++]) > EPSILON_ANGLE)
                {
                    return false;
                }	
			}
		}

		return true;
	}
	
	public void SetInputParameters(IGridModel testCase)
	{
		_busCount = testCase.GetDimension();
		
		_busModel = testCase.GetBusModel();
        _branchModel = testCase.GetBranchModel();
		_numberOfEquations = _busModel.GetNumberOfEquations();

		_jacobian = new double[_numberOfEquations][_numberOfEquations];
		_deltaS = new double[_numberOfEquations];
	}
	
	/**
	 * Poplates the Jacobian matrix with the current assumed values.
	 *
     *  i        j       Jacobian tuple
     *  Slack    Slack   [ -, -, -, - ]
     *  Slack    PQ      [ -, -, -, - ]
     *  Slack    PV      [ -, -, -, - ]
     *  PQ       Slack   [ -, -, -, - ]
     *  PQ       PQ      [ H, N, J, L ]
     *  PQ       PV      [ H, -, J, - ]
     *  PV       Slack   [ -, -, -, - ]
     *  PV       PQ      [ H, N, -, - ]
     *  PV       PV      [ H, -, -, - ]
	 */
	public void PopulateJacobian()
	{
		int verticalIndex = 0;
		int horizontalIndex = 0;
		
		boolean reactiveFound = false;
        boolean activeFound = false;
		
		for (int i = 0; i < _busCount; i++) 
		{
			horizontalIndex = 0;
			
			// iterate over the top row (populate H and N)
			for (int j = 0; j < _busCount; j++)
			{
				// slack
				if(_busModel.GetBusType(i) == BusModel.SLACK_BUS_TYPE || _busModel.GetBusType(j) == BusModel.SLACK_BUS_TYPE)
				{
					continue;
				}
				
				// PQ
				if (_busModel.GetBusType(i) == BusModel.PQ_BUS_TYPE && _busModel.GetBusType(j) == BusModel.PQ_BUS_TYPE)
				{
					_jacobian[verticalIndex][horizontalIndex++] = CheckNumber(H(i, j), i, j);
					_jacobian[verticalIndex][horizontalIndex++] = CheckNumber(N(i, j), i, j);
					activeFound = true;
                    continue;
				}
				
				// PV
				if (_busModel.GetBusType(i) == BusModel.PV_BUS_TYPE && _busModel.GetBusType(j) != BusModel.PV_BUS_TYPE)
				{
					_jacobian[verticalIndex][horizontalIndex++] = CheckNumber(H(i, j), i, j);
					_jacobian[verticalIndex][horizontalIndex++] = CheckNumber(N(i, j), i, j);
					activeFound = true;
                    continue;
				}
				
				if (_busModel.GetBusType(j) == BusModel.PV_BUS_TYPE)
				{
					_jacobian[verticalIndex][horizontalIndex++] = CheckNumber(H(i, j), i, j);
					activeFound = true;
                    continue;
				}
			}
			
			// increase the verticalIndex, if any additions were made in the current row of the jacobian
			if (activeFound)
			{
				verticalIndex++;
				activeFound = false;
			}
			
			horizontalIndex = 0;
			
			// iterate over the bottom row (populate J and L)
			for (int j = 0; j < _busCount; j++)
			{
				// slack
				if(_busModel.GetBusType(i) == BusModel.SLACK_BUS_TYPE || _busModel.GetBusType(j) == BusModel.SLACK_BUS_TYPE)
				{
					continue;
				}
				
				// PQ
				if (_busModel.GetBusType(i) == BusModel.PQ_BUS_TYPE && _busModel.GetBusType(j) == BusModel.PQ_BUS_TYPE)
				{
					_jacobian[verticalIndex][horizontalIndex++] = CheckNumber(J(i, j), i, j);
					_jacobian[verticalIndex][horizontalIndex++] = CheckNumber(L(i, j), i, j);
					reactiveFound = true;
                    continue;
				}
				
				// PV
				if (_busModel.GetBusType(i) == BusModel.PV_BUS_TYPE)
				{
					continue;
				}
				else if (_busModel.GetBusType(j) == BusModel.PV_BUS_TYPE)
				{
					_jacobian[verticalIndex][horizontalIndex++] = CheckNumber(J(i, j), i, j);
					reactiveFound = true;
					continue;
				}
			}
			
			// increase the verticalIndex, if any additions were made in the current row of the jacobian
			if (reactiveFound)
			{
				verticalIndex++;
				reactiveFound = false;
			}
		}
	}	
	
	/**
	 * Populates the deltaS matrix with the current assumed values.
	 * deltaS[k] = P_k - f_k
	 * deltaS[k+1] = Q_k - g_k
	 */
	public void PopulateDeltaS()
	{
		int deltaSIndex = 0;
		
		for (int busID = 0; busID < _busCount; busID++) 
		{
			if (_busModel.GetBusType(busID) == BusModel.PQ_BUS_TYPE)
			{
				_deltaS[deltaSIndex++] = _busModel.GetBusPg(busID) - _busModel.GetBusPd(busID) - Fk(busID);
				_deltaS[deltaSIndex++] = _busModel.GetBusQg(busID) - _busModel.GetBusQd(busID) - Gk(busID);
			}
			else if (_busModel.GetBusType(busID) == BusModel.PV_BUS_TYPE)
			{
				_deltaS[deltaSIndex++] = _busModel.GetBusPg(busID) - _busModel.GetBusPd(busID) - Fk(busID);
			}
		}
	}
	
	/**
	 * Returns the partial derivative of F_k for Theta_m.
	 * Formula: H = U_k * U_m * Y_km * (-sin(Phi_km + Theta_m - Theta_k))
	 * @param m
	 * @param n
	 * @return
	 */
	public double H(int k, int m)
	{
		if (_branchModel.GetBranchG(k, m) == 0 && _branchModel.GetBranchB(k, m) == 0) 
		{
			return 0.0;
		}
		
		if (k == m)
		{
			return -_branchModel.GetBranchB(k, k) * _busModel.GetBusVoltage(k) * _busModel.GetBusVoltage(k) - Gk(k);
		}
		else
		{
			double theta = _busModel.GetBusAngle(k) - _busModel.GetBusAngle(m);
			return _busModel.GetBusVoltage(k) * _busModel.GetBusVoltage(m) * (_branchModel.GetBranchG(k, m) * Math.sin(theta) - _branchModel.GetBranchB(k, m) * Math.cos(theta));
		}
	}
	
	/**
	 * Returns the partial derivative of F_k for U_m.
	 * Formula: N = U_k * Y_km * cos(Phi_km + Theta_m - Theta_k)
	 * @param m
	 * @param n
	 * @return
	 */
	public double N(int k, int m)
	{
		if (_branchModel.GetBranchG(k, m) == 0 && _branchModel.GetBranchB(k, m) == 0) 
		{
			return 0.0;
		}
		
		if (k == m)
		{
			return _branchModel.GetBranchG(k, k) * _busModel.GetBusVoltage(k) * _busModel.GetBusVoltage(k) + Fk(k);
		}
		else
		{
			double theta = _busModel.GetBusAngle(k) - _busModel.GetBusAngle(m);
			return _busModel.GetBusVoltage(k) * _busModel.GetBusVoltage(m) * (_branchModel.GetBranchG(k, m) * Math.cos(theta) + _branchModel.GetBranchB(k, m) * Math.sin(theta));
		}		
	}
	
	/**
	 * Returns the partial derivative of G_k for Theta_m.
	 * Formula: J = U_k * U_m * Y_km * cos(Phi_km + Theta_m - Theta_k)
	 * @param m
	 * @param n
	 * @return
	 */
	public double J(int k, int m)
	{
		if (_branchModel.GetBranchG(k, m) == 0 && _branchModel.GetBranchB(k, m) == 0) 
		{
			return 0.0;
		}
		
		if (k == m)
		{
			return -_branchModel.GetBranchG(k, k) * _busModel.GetBusVoltage(k) * _busModel.GetBusVoltage(k) + Fk(k);
		}
		else
		{
			double theta = _busModel.GetBusAngle(k) - _busModel.GetBusAngle(m);
			return -_busModel.GetBusVoltage(k) * _busModel.GetBusVoltage(m) * (_branchModel.GetBranchG(k, m) * Math.cos(theta) + _branchModel.GetBranchB(k, m) * Math.sin(theta));
		}	
	}
	
	/**
	 * Returns the partial derivative of G_k for U_m.
	 * Formula: L = U_k * Y_km * sin(Phi_km + Theta_m - Theta_k)
	 * @param m
	 * @param n
	 * @return
	 */
	public double L(int k, int m)
	{
		if (_branchModel.GetBranchG(k, m) == 0 && _branchModel.GetBranchB(k, m) == 0) 
		{
			return 0.0;
		}
		
		if (k == m)
		{
			return -_branchModel.GetBranchB(k, k) * _busModel.GetBusVoltage(k) * _busModel.GetBusVoltage(k) + Gk(k);
		}
		else
		{
			double theta = _busModel.GetBusAngle(k) - _busModel.GetBusAngle(m);
			return _busModel.GetBusVoltage(k) * _busModel.GetBusVoltage(m) * (_branchModel.GetBranchG(k, m) * Math.sin(theta) - _branchModel.GetBranchB(k, m) * Math.cos(theta));
		}
	}
	
	
	/**
	 * Active power, formula: P_k = V_k * V_0 * (G_k0 * cos(theta_k - theta_0) + B_k0 * sin(theta_k - theta_0)) + V_k * sum_{i=1}^{N} (V_i * (G_ki * cos(theta_k - theta_i) + B_ki * sin(theta_k - theta_i)))
	 * @param k Index
	 * @return Active power
	 */
	public double Fk(int k)
	{
		double g = 0;
		double b = 0;
		
		double sum = 0;
		double theta = 0;
		
		for (int busID = 0; busID < _busCount; busID++) 
		{
			g = _branchModel.GetBranchG(k, busID);
			b = _branchModel.GetBranchB(k, busID);
			
			if(g == 0 && b == 0)
			{
				continue;
			}
			
			theta = _busModel.GetBusAngle(k) - _busModel.GetBusAngle(busID);
			sum += _busModel.GetBusVoltage(busID) * (g * Math.cos(theta) + b * Math.sin(theta));
		}
		
		return _busModel.GetBusVoltage(k) * sum;
	}
	
	/**
	 * Rective power, formula: Q_k = V_k * V_0 * (G_k0 * sin(theta_k - theta_0) - B_k0 * cos(theta_k - theta_0)) + V_k * sum_{i=0}^{N} (V_i * (G_ki * sin(theta_k - theta_i) - B_ki * cos(theta_k - theta_i)))
	 * @param k Index
	 * @return Reactive power
	 */
	public double Gk(int k)
	{	
		double g = 0;
		double b = 0;
		
		double sum = 0;
		double theta = 0;
		
		for (int busID = 0; busID < _busCount; busID++) 
		{
			g = _branchModel.GetBranchG(k, busID);
			b = _branchModel.GetBranchB(k, busID);
			
			if(g == 0 && b == 0)
			{
				continue;
			}
			
			theta = _busModel.GetBusAngle(k) - _busModel.GetBusAngle(busID);
			sum += _busModel.GetBusVoltage(busID) * (g * Math.sin(theta) - b * Math.cos(theta));
		}
		
		return _busModel.GetBusVoltage(k) * sum;
	}
}
