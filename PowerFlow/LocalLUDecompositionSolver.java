package SparkPowerCalculations.PowerFlow;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.DecompositionSolver;
import org.apache.commons.math3.linear.LUDecomposition;

public class LocalLUDecompositionSolver implements ISolver 
{
	@Override
	public void Solve(double[][] A, double[] x, double[] b) 
	{	
		DecompositionSolver solver = new LUDecomposition(new Array2DRowRealMatrix(A, false), 1e-31).getSolver();
		System.arraycopy(solver.solve(new ArrayRealVector(b, false)).toArray(), 0, x, 0, x.length);
	}

}
