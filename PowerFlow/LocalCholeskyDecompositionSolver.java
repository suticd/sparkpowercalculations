package SparkPowerCalculations.PowerFlow;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.CholeskyDecomposition;
import org.apache.commons.math3.linear.DecompositionSolver;

public class LocalCholeskyDecompositionSolver implements ISolver {

	@Override
	public void Solve(double[][] A, double[] x, double[] b) 
	{		
		DecompositionSolver solver = new CholeskyDecomposition(new Array2DRowRealMatrix(A, false)).getSolver();
		System.arraycopy(solver.solve(new ArrayRealVector(b, false)).toArray(), 0, x, 0, x.length);
	}

}
