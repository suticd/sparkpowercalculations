package SparkPowerCalculations;

import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaSparkContext;

public final class SparkContextProvider 
{
	private static JavaSparkContext _sparkContext = null;
	
	private static boolean _executeLocal = true;
	private static boolean _useGetOrCreate = false;
	
	private SparkContextProvider()
	{		
	}
	
	public static void SetSparkContextParameters(boolean executeLocal, boolean useGetOrCreate)
	{
		_executeLocal = executeLocal;
		_useGetOrCreate = useGetOrCreate;
	}
	
	public static JavaSparkContext GetSparkContext()
	{
		if (_sparkContext == null)
		{
			if(_useGetOrCreate)
			{
				_sparkContext = JavaSparkContext.fromSparkContext(SparkContext.getOrCreate());
			}
			else
			{
				SparkConf conf;
				if (_executeLocal)
				{
					conf = new SparkConf().setAppName("SparkPowerCalculations").setMaster("local[*]");
				}
				else
				{
					conf = new SparkConf().setAppName("SparkPowerCalculations");
				}
				
				_sparkContext = new JavaSparkContext(conf);
			}						
		}
		
		return _sparkContext;
	}
}
