package SparkPowerCalculations.ContingencyAnalysis;

import java.util.LinkedList;
import java.util.List;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.linalg.distributed.BlockMatrix;
import org.apache.spark.mllib.linalg.distributed.CoordinateMatrix;
import org.apache.spark.mllib.linalg.distributed.IndexedRow;
import org.apache.spark.mllib.linalg.distributed.IndexedRowMatrix;
import org.apache.spark.mllib.linalg.distributed.MatrixEntry;
import org.junit.Assert;
import org.junit.Test;

import SparkPowerCalculations.SparkContextProvider;
import SparkPowerCalculations.TestCases.GridModel;

public class DistributedMatrixTopologyAnalizerTests 
{
	private final static String DATA_PATH = "/SparkPowerCalculations/TestCases/data_files/";
	
	private static int counter;
	
	@Test
	public void GenerateConnectivityMatrix_14case_NoOutages()
	{
		String filePath = DATA_PATH + "case_14.m";
		
		GridModel testCase = new GridModel();
		testCase.Initialize(filePath);
		
		DistributedMatrixTopologyAnalizer sut = new DistributedMatrixTopologyAnalizer();
		BlockMatrix mb = sut.GenerateConnectivityMatrix(testCase.GetBranchModel(), testCase.GetBusModel());
		
		BlockMatrix mb2 = mb.multiply(mb);
		mb2.toIndexedRowMatrix().rows().collect();
	}
	
	@Test
	public void FindIslandMatrix_14case_NoOutages_FullyConnected()
	{
		String filePath = DATA_PATH + "case_14.m";
		
		GridModel testCase = new GridModel();
		testCase.Initialize(filePath);
		
		DistributedMatrixTopologyAnalizer sut = new DistributedMatrixTopologyAnalizer();
		BlockMatrix mb = sut.GenerateConnectivityMatrix(testCase.GetBranchModel(), testCase.GetBusModel());
		
		CoordinateMatrix result = sut.FindIslandMatrix(mb);
		
		JavaRDD<MatrixEntry> matrixEntries = result.entries().toJavaRDD();
		List<MatrixEntry> list = matrixEntries.collect();
		for (MatrixEntry matrixEntry : list) 
		{
			Assert.assertEquals(1.0, matrixEntry.value(), 0.00001);
			//System.out.println("(" + matrixEntry.i() + ", " + matrixEntry.j() + ") = " + matrixEntry.value());
		}
	}
	
	@Test
	public void FindIslandMatrix_300case_NoOutages_FullyConnected()
	{
		String filePath = DATA_PATH + "case_300.m";
		
		GridModel testCase = new GridModel();
		testCase.Initialize(filePath);
		
		DistributedMatrixTopologyAnalizer sut = new DistributedMatrixTopologyAnalizer();
		BlockMatrix mb = sut.GenerateConnectivityMatrix(testCase.GetBranchModel(), testCase.GetBusModel());
		
		CoordinateMatrix result = sut.FindIslandMatrix(mb);
		
		JavaRDD<MatrixEntry> matrixEntries = result.entries().toJavaRDD();
		List<MatrixEntry> list = matrixEntries.collect();
		for (MatrixEntry matrixEntry : list) 
		{
			Assert.assertEquals(1.0, matrixEntry.value(), 0.00001);
			//System.out.println("(" + matrixEntry.i() + ", " + matrixEntry.j() + ") = " + matrixEntry.value());
		}
	}
	
	@Test
	public void FindIslandMatrix_3012case_NoOutages_FullyConnected()
	{
		String filePath = DATA_PATH + "case_3012.m";
		
		GridModel testCase = new GridModel();
		testCase.Initialize(filePath);
		
		DistributedMatrixTopologyAnalizer sut = new DistributedMatrixTopologyAnalizer();
		BlockMatrix mb = sut.GenerateConnectivityMatrix(testCase.GetBranchModel(), testCase.GetBusModel());
		
		CoordinateMatrix result = sut.FindIslandMatrix(mb);
		
		JavaRDD<MatrixEntry> matrixEntries = result.entries().toJavaRDD();
		List<MatrixEntry> list = matrixEntries.collect();
		for (MatrixEntry matrixEntry : list) 
		{
			Assert.assertEquals(1.0, matrixEntry.value(), 0.00001);
			//System.out.println("(" + matrixEntry.i() + ", " + matrixEntry.j() + ") = " + matrixEntry.value());
		}
	}
	
	@Test
	public void AllEntriesAreOnes_FullyConnected_ReturnsTrue()
	{
		int dimension = 30;
		double[] array = new double[dimension];
		
		for (int i = 0; i < dimension; i++) 
		{
			array[i] = 1;
		}
		
		Vector vector = Vectors.dense(array);
		
		LinkedList<IndexedRow> indexedRows = new LinkedList<IndexedRow>();
		
		for (int i = 0; i < dimension; i++) 
		{
			indexedRows.add(new IndexedRow(i, vector));
		}
		
		JavaRDD<IndexedRow> indexedRowsRdd = SparkContextProvider.GetSparkContext().parallelize(indexedRows);
		IndexedRowMatrix irMatrix = new IndexedRowMatrix(indexedRowsRdd.rdd());
		BlockMatrix mb = irMatrix.toBlockMatrix();
		
		DistributedMatrixTopologyAnalizer sut = new DistributedMatrixTopologyAnalizer();
		
		boolean result = sut.AllEntriesAreOnes(mb);
		
		Assert.assertEquals(true, result);
	}
	
	@Test
	public void AllEntriesAreOnes_NotConnected_ReturnsFalse()
	{
		int dimension = 30;
		double[] array = new double[dimension];
		
		for (int i = 0; i < dimension; i++) 
		{
			array[i] = 1;
		}
		
		array[4] = 0;
		
		Vector vector = Vectors.dense(array);
		
		LinkedList<IndexedRow> indexedRows = new LinkedList<IndexedRow>();
		
		for (int i = 0; i < dimension; i++) 
		{
			indexedRows.add(new IndexedRow(i, vector));
		}
		
		JavaRDD<IndexedRow> indexedRowsRdd = SparkContextProvider.GetSparkContext().parallelize(indexedRows);
		IndexedRowMatrix irMatrix = new IndexedRowMatrix(indexedRowsRdd.rdd());
		BlockMatrix mb = irMatrix.toBlockMatrix();
		
		DistributedMatrixTopologyAnalizer sut = new DistributedMatrixTopologyAnalizer();
		
		boolean result = sut.AllEntriesAreOnes(mb);
		
		Assert.assertEquals(false, result);
	}
	
	//@Test
	public void RunAll_NoOutages()
	{
		NoOutagesCommonTest(4);
		NoOutagesCommonTest(5);	
		NoOutagesCommonTest(6);	
		NoOutagesCommonTest(9);	
		NoOutagesCommonTest(14);
		NoOutagesCommonTest(24);	
		NoOutagesCommonTest(30);
		NoOutagesCommonTest(39);	
		NoOutagesCommonTest(57);	
		NoOutagesCommonTest(89);	
		NoOutagesCommonTest(118);
		NoOutagesCommonTest(300);	
		NoOutagesCommonTest(1354);	
		NoOutagesCommonTest(2383);
		NoOutagesCommonTest(2736);	
		NoOutagesCommonTest(2746);
		NoOutagesCommonTest(2869);	
		NoOutagesCommonTest(3012);	
		NoOutagesCommonTest(3120);
		NoOutagesCommonTest(3374);
		NoOutagesCommonTest(9241);
	}
	
	@Test
	public void PerformConnectednessAnalysis_4case_NoOutages()
	{
		NoOutagesCommonTest(4);
	}
	
	@Test
	public void PerformConnectednessAnalysis_5case_NoOutages()
	{
		NoOutagesCommonTest(5);
	}
	
	@Test
	public void PerformConnectednessAnalysis_6case_NoOutages()
	{
		NoOutagesCommonTest(6);
	}
	
	@Test
	public void PerformConnectednessAnalysis_9case_NoOutages()
	{
		NoOutagesCommonTest(9);
	}
	
	@Test
	public void PerformConnectednessAnalysis_14case_NoOutages()
	{
		NoOutagesCommonTest(14);
	}
	
	@Test
	public void PerformConnectednessAnalysis_24case_NoOutages()
	{
		NoOutagesCommonTest(24);
	}
	
	@Test
	public void PerformConnectednessAnalysis_30case_NoOutages()
	{
		NoOutagesCommonTest(30);
	}
	
	@Test
	public void PerformConnectednessAnalysis_39case_NoOutages()
	{
		NoOutagesCommonTest(39);
	}
	
	@Test
	public void PerformConnectednessAnalysis_57case_NoOutages()
	{
		NoOutagesCommonTest(57);
	}
	
	@Test
	public void PerformConnectednessAnalysis_89case_NoOutages()
	{
		NoOutagesCommonTest(89);
	}
	
	@Test
	public void PerformConnectednessAnalysis_118case_NoOutages()
	{
		NoOutagesCommonTest(118);
	}
	
	@Test
	public void PerformConnectednessAnalysis_300case_NoOutages()
	{
		NoOutagesCommonTest(300);
	}
	
	@Test
	public void PerformConnectednessAnalysis_1354case_NoOutages()
	{
		NoOutagesCommonTest(1354);
	}
	
	@Test
	public void PerformConnectednessAnalysis_2383case_NoOutages()
	{
		NoOutagesCommonTest(2383);
	}
	
	@Test
	public void PerformConnectednessAnalysis_2736case_NoOutages()
	{
		NoOutagesCommonTest(2736);
	}
	
	@Test
	public void PerformConnectednessAnalysis_2746case_NoOutages()
	{
		NoOutagesCommonTest(2746);
	}
	
	@Test
	public void PerformConnectednessAnalysis_2869case_NoOutages()
	{
		NoOutagesCommonTest(2869);
	}
	
	@Test
	public void PerformConnectednessAnalysis_3012case_NoOutages()
	{
		NoOutagesCommonTest(3012);
	}
	
	@Test
	public void PerformConnectednessAnalysis_3120case_NoOutages()
	{
		NoOutagesCommonTest(3120);
	}
	
	@Test
	public void PerformConnectednessAnalysis_3374case_NoOutages()
	{
		NoOutagesCommonTest(3374);
	}
	
	@Test
	public void PerformConnectednessAnalysis_9241case_NoOutages()
	{
		NoOutagesCommonTest(9241);
	}
	
	@Test
	public void PerformConnectednessAnalysis_14case_OneOutage()
	{
		String filePath = DATA_PATH + "case_" + 14 + ".m";
		GridModel testCase = new GridModel();
		testCase.Initialize(filePath);
		testCase.GetBranchModel().CreateOutage(6, 7);
		
		DistributedMatrixTopologyAnalizer sut = new DistributedMatrixTopologyAnalizer();
		List<List<Long>> list = sut.PerformConnectednessAnalysis(testCase.GetBranchModel(), testCase.GetBusModel());
		
		Assert.assertEquals(2, list.size());
	}
	
	@Test
	public void PerformConnectednessAnalysis_4case_TwoOutages()
	{
		String filePath = DATA_PATH + "case_" + 4 + ".m";
		GridModel testCase = new GridModel();
		testCase.Initialize(filePath);
		testCase.GetBranchModel().CreateOutage(0, 1);
		testCase.GetBranchModel().CreateOutage(2, 3);
		
		DistributedMatrixTopologyAnalizer sut = new DistributedMatrixTopologyAnalizer();
		List<List<Long>> list = sut.PerformConnectednessAnalysis(testCase.GetBranchModel(), testCase.GetBusModel());
		
		Assert.assertEquals(2, list.size());
	}
	
	public void NoOutagesCommonTest(int numberOfBuses)
	{
		String filePath = DATA_PATH + "case_" + numberOfBuses + ".m";
		GridModel testCase = new GridModel();
		testCase.Initialize(filePath);
		
		DistributedMatrixTopologyAnalizer sut = new DistributedMatrixTopologyAnalizer();
		List<List<Long>> list = sut.PerformConnectednessAnalysis(testCase.GetBranchModel(), testCase.GetBusModel());
		
		Assert.assertEquals(1, list.size());
		
		counter = 0;
		list.get(0).forEach(o -> counter++);		
		Assert.assertEquals(numberOfBuses, counter);
	}
}
