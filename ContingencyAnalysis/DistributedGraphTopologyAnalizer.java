package SparkPowerCalculations.ContingencyAnalysis;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.graphx.*;
import org.apache.spark.storage.StorageLevel;

import SparkPowerCalculations.SparkContextProvider;
import SparkPowerCalculations.TestCases.BranchElement;
import SparkPowerCalculations.TestCases.BranchModel;
import SparkPowerCalculations.TestCases.BusModel;
import scala.Tuple2;
import scala.reflect.ClassTag;

import java.util.LinkedList;
import java.util.List;

public class DistributedGraphTopologyAnalizer implements ITopologyAnalyzer
{
	private static final long serialVersionUID = -4438894271610018255L;

	public List<List<Long>> PerformConnectednessAnalysis(BranchModel branchModel, BusModel busModel)
	{
		long startTime = System.nanoTime();
		JavaSparkContext sc = SparkContextProvider.GetSparkContext();
		
		BranchElement[] branches = branchModel.GetBranches();
		LinkedList<Edge<Integer>> edgeList = new LinkedList<Edge<Integer>>();
		LinkedList<Tuple2<Object, Integer>> vertexList = new LinkedList<Tuple2<Object, Integer>>();

		for (int i = 0; i < busModel.GetNumberOfBuses(); i++)
		{
			vertexList.add(new Tuple2<Object, Integer>((long)busModel.GetBusID(i), busModel.GetBusType(i)));
		}
		
		for (int i = 0; i < branches.length; i++) 
		{
			if(branches[i].BranchStatus != 0)
			{
				edgeList.add(new Edge<Integer>((long)(busModel.GetBusID(branches[i].FromBus)), (long)(busModel.GetBusID(branches[i].ToBus)), 0));
			}
		}
		
		JavaRDD<Edge<Integer>> edges = sc.parallelize(edgeList, 800);
		JavaRDD<Tuple2<Object, Integer>> vertices = sc.parallelize(vertexList, 800);
		
		ClassTag<Integer> uClassTag = scala.reflect.ClassTag$.MODULE$.apply(Integer.class);
		Graph<Integer, Integer> graph = Graph.apply(vertices.rdd(), edges.rdd(), (int)0, StorageLevel.MEMORY_ONLY(), StorageLevel.MEMORY_ONLY(), uClassTag, uClassTag);

		Graph<Object, Integer> connectedComponentsGraph = graph.ops().connectedComponents();
		
		List<List<Long>> retList = connectedComponentsGraph.vertices().toJavaRDD().mapToPair(x -> x.swap()).groupByKey().map(t -> ListFromIterable(t._2)).collect();
		
		long stopTime = System.nanoTime();
		System.out.println("Connectedness analysis performed in: " + (stopTime - startTime) + "[ns].");
		return retList;
	}
	
	private List<Long> ListFromIterable(Iterable<Object> iterable) 
	{
		List<Long> list = new LinkedList<Long>();
	    for (Object item : iterable) 
	    {
	        list.add((Long)item);
	    }
	    return list;
	}
}
