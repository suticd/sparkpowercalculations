package SparkPowerCalculations.ContingencyAnalysis;

import java.util.LinkedList;
import java.util.List;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.linalg.distributed.BlockMatrix;
import org.apache.spark.mllib.linalg.distributed.CoordinateMatrix;
import org.apache.spark.mllib.linalg.distributed.MatrixEntry;

import SparkPowerCalculations.SparkContextProvider;
import SparkPowerCalculations.TestCases.BranchElement;
import SparkPowerCalculations.TestCases.BranchModel;
import SparkPowerCalculations.TestCases.BusModel;

public class DistributedMatrixTopologyAnalizer implements ITopologyAnalyzer
{
	private static final long serialVersionUID = 1378955357336954965L;

	public List<List<Long>> PerformConnectednessAnalysis(BranchModel branchModel, BusModel busModel)
	{
		long startTime = System.nanoTime();
		BlockMatrix connectivityMatrixA = GenerateConnectivityMatrix(branchModel, busModel);
		CoordinateMatrix islandMatrix = FindIslandMatrix(connectivityMatrixA);
		List<List<Long>> retList = null;
		try 
		{
			retList = FindIslands(islandMatrix);
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
		long stopTime = System.nanoTime();
		System.out.println("Connectedness analysis performed in: " + (stopTime - startTime) + "[ns].");
		
		return retList;
	}
	
	public BlockMatrix GenerateConnectivityMatrix(BranchModel branchModel, BusModel busModel)
	{
		LinkedList<MatrixEntry> matrixEntries = new LinkedList<MatrixEntry>();
		
		BranchElement[] branches = branchModel.GetBranches();
		
		for (int i = 0; i < branches.length; i++) 
		{
			if(branches[i].BranchStatus == 1)
			{
				matrixEntries.add(new MatrixEntry(branches[i].FromBus, branches[i].ToBus, 1));
				matrixEntries.add(new MatrixEntry(branches[i].ToBus, branches[i].FromBus, 1));
			}
		}
		
		for (int i = 0; i < busModel.GetNumberOfBuses(); i++) 
		{
			matrixEntries.add(new MatrixEntry(i, i, 1));
		}
		
		JavaSparkContext sc = SparkContextProvider.GetSparkContext();
		JavaRDD<MatrixEntry> matrixEntriesRDD = sc.parallelize(matrixEntries, 100);
		
		CoordinateMatrix coordinateMatrix = new CoordinateMatrix(matrixEntriesRDD.rdd(), busModel.GetNumberOfBuses(), busModel.GetNumberOfBuses());
		
		return coordinateMatrix.toBlockMatrix();
	}
	
	public CoordinateMatrix FindIslandMatrix(BlockMatrix connectivityMatrix)
	{
		BlockMatrix connectivityMatrixA = connectivityMatrix;
		BlockMatrix connectivityMatrixB = connectivityMatrix;
		
		int numberOfIterations = 0;
		int maxNumberOfMultiplications = (int)(Math.log(connectivityMatrix.numRows() - 2) / Math.log(2.0)) + 1;
		
		while (numberOfIterations != maxNumberOfMultiplications) 
		{
			connectivityMatrixA = connectivityMatrixB.multiply(connectivityMatrixB).cache();
			connectivityMatrixA = SetNonZeroesToOnes(connectivityMatrixA).cache();
			
			if(AllEntriesAreOnes(connectivityMatrixA))
			{
				break;
			}
			
			numberOfIterations++;
			
			connectivityMatrixB = connectivityMatrixA;
		}
		
		return connectivityMatrixA.toCoordinateMatrix();
	}
	
	public List<List<Long>> FindIslands(CoordinateMatrix islandMatrix)
	{
		List<List<Long>> retList = new LinkedList<List<Long>>();
		
		List<Long> islandList = new LinkedList<Long>();
        List<Long> visitedList = new LinkedList<Long>();

        islandList.add(0L);
        //visitedList.add(0L);

        int currentRow = 0;
        long matrixDimension = islandMatrix.numCols();
        
        // iterate while there are unvisited rows
        while (visitedList.size() != matrixDimension)
        {
        	List<Long> localList = new LinkedList<Long>();
        	
        	currentRow = Math.toIntExact(islandList.get(0));          
            
            List<MatrixEntry> rowEntries = islandMatrix.entries().toJavaRDD().filter(matrixEntry -> matrixEntry.i() == islandList.get(0)).collect();
            islandList.remove(currentRow);
            
            int rowIterator = 0;
            
            for (int i = 0; i < matrixDimension; i++) 
            {        	
				if(rowEntries.get(rowIterator).j() == i && rowEntries.get(rowIterator).value() == 1)
				{
					localList.add((long)i);
					
					if (!visitedList.contains(i))
                    {
                        visitedList.add((long)i);
                    }
					
					if(rowIterator < rowEntries.size() - 1)
					{
						rowIterator++;
					}
				}
				else
                {
                    if (!islandList.contains(i) && !visitedList.contains(i))
                    {
                        islandList.add((long) i);
                    }
                }
			}
                     
            retList.add(localList);
        }
		
		return retList;
	}
	
	public BlockMatrix SetNonZeroesToOnes(BlockMatrix matrix)
	{
		JavaRDD<MatrixEntry> matrixEntries = matrix.toCoordinateMatrix().entries().toJavaRDD();
		
		matrixEntries = matrixEntries.map(t -> 
		{
			if(t.value() != 0)
			{
				return new MatrixEntry(t.i(), t.j(), 1);
			}
			
			return new MatrixEntry(t.i(), t.j(), 0);
		});
		
		CoordinateMatrix coordMat = new CoordinateMatrix(matrixEntries.rdd(), matrix.numRows(), matrix.numCols());
		return coordMat.toBlockMatrix();
	}
	
	public boolean AllEntriesAreOnes(BlockMatrix matrix)
	{
		JavaRDD<Boolean> blockOnes = matrix.blocks().toJavaRDD().map(t -> 
		{
			for (int i = 0; i < t._2().numRows(); i++) 
			{
				for (int j = 0; j < t._2().numCols(); j++) 
				{
					if(t._2().apply(i, j) == 0)
					{
						return false;
					}
				}
			}
			
			return true;
		});
		
		boolean retVal = blockOnes.reduce((a, b) -> a & b);
		
		return retVal;
	}
}
