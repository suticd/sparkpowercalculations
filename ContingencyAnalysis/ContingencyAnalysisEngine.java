package SparkPowerCalculations.ContingencyAnalysis;

import SparkPowerCalculations.PowerFlow.ISolver;
import SparkPowerCalculations.PowerFlow.PowerFlowEngine;
import SparkPowerCalculations.TestCases.IGridModel;

public class ContingencyAnalysisEngine 
{
	private PowerFlowEngine _loadFLow;
	
	public ContingencyAnalysisEngine(ISolver solver)
	{
		_loadFLow = new PowerFlowEngine(solver);
	}
	
	public void CalculateBaseCase(IGridModel gridModel)
	{
		double[] baseCase = _loadFLow.ExecuteLoadFlowCalculation(gridModel);
		gridModel.SetBaseCase(baseCase);
	}
	
	public double[] PerformContingencyAnalysis(IGridModel gridModel, int from, int to)
	{
		System.out.println("Running Contingency analysis for contingency from " + from + " to " + to);
		
		CalculateBaseCase(gridModel);
		
		// update admittance matrix
		gridModel.GetBranchModel().CreateOutage(from, to);
		
		// calculate load flow
		double[] outageCase =_loadFLow.ExecuteLoadFlowCalculation(gridModel);
		
		// return percentage deviance from base case
		return CalculateDeviation(outageCase, gridModel.GetBaseCase());
	}
	
	private double[] CalculateDeviation(double[] outageCase, double[] baseCase)
	{
		double[] retVal = new double[outageCase.length];
		
		for (int i = 0; i < retVal.length; i++) 
		{
			retVal[i] = (outageCase[i] - baseCase[i]) / Math.abs(baseCase[i]);
		}
		
		return retVal;
	}
}
