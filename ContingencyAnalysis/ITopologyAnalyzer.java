package SparkPowerCalculations.ContingencyAnalysis;

import java.io.Serializable;
import java.util.List;

import SparkPowerCalculations.TestCases.BranchModel;
import SparkPowerCalculations.TestCases.BusModel;

public interface ITopologyAnalyzer extends Serializable
{
	List<List<Long>> PerformConnectednessAnalysis(BranchModel branchModel, BusModel busModel);
}
